from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from accounts.models import User,Section, Location

def index(request):
	if request.user.is_authenticated:
		return redirect("/dashboard")
	return render(request,'loginpage/index.html')

def loginuser(request):
	if request.method == 'POST':
		user=authenticate(username=request.POST['username'],password=request.POST['password'])
		if user is None:
			messages.warning(request, 'User not Present')
			return render(request,'loginpage/loginuser.html')
		if not user.is_staff:
			messages.warning(request, 'User not activated by admin')
			return render(request,'loginpage/loginuser.html')
		if user is not None:
			login(request, user)
			if request.GET.get('next'):
				return redirect(request.GET.get('next'))
			return redirect('/')
	return render(request,'loginpage/loginuser.html')

def logoutuser(request):
	logout(request)
	if request.GET.get('next'):
		return redirect(request.GET.get('next'))
	return redirect('/')

def register(request):
	if request.method == 'POST':
		if request.POST['password1'] != request.POST['password2']:
			messages.error(request,'Passwords dont match')
			return redirect('/loginpage/register')
		if User.objects.filter(username=request.POST['username']):
			messages.error(request,'UserName already present')
			return redirect('/loginpage/register')
		user = User.objects.create_user(username=request.POST['username'], 
			password=request.POST['password1'],)
		user.first_name = request.POST['first_name']
		user.last_name = request.POST['last_name']
		user.section = Section.objects.get(pk=request.POST['section'])
		user.location = Location.objects.get(pk=request.POST['location'])
		user.save()
		messages.info(request, 'User will get activated by admin')
		return redirect('/loginpage')
	context = {'sections':Section.objects.all(),'location':Location.objects.all()}
	return render(request,'loginpage/register.html',context)


def changepassword(request):
	if request.method == 'POST':
		if request.POST['password1'] != request.POST['password2']:
			messages.error(request,'Passwords dont match')
		user = User.objects.get(username__exact=request.POST['username'])
		user.set_password(request.POST['password1'])
		user.save()
		messages.success(request,'Password changed')
		return redirect('/loginpage/')
	return render(request,'loginpage/changepassword.html')
