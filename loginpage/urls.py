from django.urls import path
from .import views

app_name='loginpage'

urlpatterns = [
	# path('',views.index, name='index'),
	path('',views.loginuser,name='loginuser'),
	path('logout',views.logoutuser,name='logoutuser'),
	path('register',views.register,name='register'),
	path('changepassword',views.changepassword, name='changepassword'),
]