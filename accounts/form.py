from django import forms
from .models import *


class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = '__all__'

class ItemForm(forms.ModelForm):
	class Meta:
		model = Item
		fields = '__all__'

class SectionForm(forms.ModelForm):
	class Meta:
		model = Section
		fields = '__all__'

class IssueForm(forms.ModelForm):
	class Meta:
		model = Issue
		fields = '__all__'		

class EmailForm(forms.ModelForm):
	class Meta:
		model = EmailAccounts
		fields = '__all__'

class LocationForm(forms.ModelForm):
	class Meta:
		model = Location
		fields = '__all__'