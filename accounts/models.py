from django.db import models
from django.contrib.auth.models import AbstractUser,AbstractBaseUser, BaseUserManager
from .ModelManager import UserManager
import datetime
from django.utils.timezone import now
import os

class Section(models.Model):
	section_name = models.CharField(max_length=50,unique=True)
	def __str__(self):
		return self.section_name

class Location(models.Model):
	location_name = models.CharField(max_length=100)

	def __str__(self):
		return self.location_name

class User(AbstractUser):
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	username = models.CharField(max_length=100,unique=True)
	section = models.ForeignKey(Section, on_delete=models.RESTRICT,null=True)
	location = models.ForeignKey(Location, on_delete=models.RESTRICT, null=True)

	def __str__(self):
		return self.username


class Category(models.Model):
	category_name = models.CharField(max_length=50,unique=True)
	location = models.ForeignKey(Location,on_delete=models.RESTRICT,null=True)
	section = models.ForeignKey(Section,on_delete=models.RESTRICT,null=True)
	def __str__(self):
		return self.category_name

class Item(models.Model):
	category = models.ForeignKey(Category,on_delete=models.RESTRICT,null=True)
	item_name = models.CharField(max_length=50,unique=True)
	item_count = models.IntegerField(default=0,null=True,blank=True)
	def __str__(self):
		return self.item_name

class Entry(models.Model):
	seller_name = models.CharField(max_length=20)
	category = models.ForeignKey(Category,on_delete=models.RESTRICT)
	item = models.TextField(null=True,blank=True)
	remarks = models.TextField(null=True,blank=True)
	purchase_doc = models.FileField(upload_to='items/purchase_docs/',null=True,blank=True)
	additional_doc = models.FileField(upload_to='items/additional_doc/', null=True,blank=True)
	date_of_entry = models.DateField(default=now,null=True,blank=True)
	entered_by = models.ForeignKey(User, on_delete=models.RESTRICT)
	ref_no = models.CharField(max_length=100,null=True,blank=True)

	def __str__(self):
		return self.seller_name


class Issue(models.Model):
	name = models.CharField(max_length=200,null=True,blank=True)
	designation = models.CharField(max_length=200,null=True,blank=True)
	category = models.ForeignKey(Category,on_delete=models.RESTRICT)
	item = models.TextField(null=True,blank=True)
	section = models.ForeignKey(Section,on_delete=models.RESTRICT)
	date_of_issue = models.DateField(default=now)
	remarks = models.TextField(null=True,blank=True)
	issued_by = models.ForeignKey(User, on_delete=models.RESTRICT)
	requisition = models.FileField(upload_to='items/requisition/',null=True,blank=True)
	def __str__(self):
		return self.name

class EmailAccounts(models.Model):
	email = models.CharField(max_length=100)
	active = models.BooleanField(default=True)
	section = models.ForeignKey(Section,on_delete=models.RESTRICT,null=True)
	def __str__(self):
		return self.email

