from django.urls import path
from .import views

app_name='accounts'

urlpatterns = [
	path('',views.index, name='index'),
	path('item',views.item, name='item'),
	path('edititem/<int:pk>', views.edititem, name='edititem'),
	path('section',views.section, name='section'),
	path('email',views.email, name='email'),
	path('locations',views.locations, name='locations'),
]