# Generated by Django 3.2.9 on 2024-04-25 06:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20240319_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entry',
            name='ref_no',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
