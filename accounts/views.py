from django.shortcuts import render,redirect
from .form import *
from .models import *
from django.http import HttpResponse
from django.contrib import messages
from django.contrib.auth.decorators import login_required

@login_required(login_url='/loginpage')
def index(request):
	if request.method == 'POST':
		if request.POST.get('name'):
			category = Category.objects.get(pk=request.POST['pk'])
			category.category_name = request.POST['name']
			category.save()
		else:
			form = CategoryForm(request.POST)
			if form.is_valid():
				form.save()
			else:
				return HttpResponse(form.errors)
		return redirect('/accounts')
	context = {'sections':Section.objects.all()}
	if not request.user.is_superuser:
		context['categories']=Category.objects.filter(section=request.user.section)
	else:
		context['categories']=Category.objects.all()
	context['location'] = Location.objects.all()
	return render(request,'accounts/index.html', context)

@login_required(login_url='/loginpage')
def item(request):
	if request.method == 'POST':
		subcategorypresent = Item.objects.filter(item_name__iexact=request.POST['item_name'])
		if len(subcategorypresent)>0:
			messages.error(request, 'SubCategory ALREADY PRESENT')
			return redirect('/accounts/item')
		form = ItemForm(request.POST)
		if form.is_valid():
			form.save()
		else:
			return HttpResponse(str(form.errors))
		return redirect('/accounts/item')
	if request.user.is_superuser:
		categories = Category.objects.all()
	else:
		categories = Category.objects.filter(section=request.user.section)
	items = Item.objects.filter(category__in=categories)
	context = {'categories':categories,'items':items}
	return render(request,'accounts/item.html', context)

@login_required(login_url='/loginpage')
def edititem(request,pk):
	if request.method == 'POST':
		form = ItemForm(request.POST,instance=Item.objects.get(pk=pk))
		if form.is_valid():
			form.save()
			return redirect('/accounts/item')
		else:
			return HttpResponse(form.errors)
	return render(request,'accounts/edititem.html',{'categories':Category.objects.all(),
		'item':Item.objects.get(pk=pk)})

@login_required(login_url='/loginpage')
def section(request):
	if request.method == 'POST':
		if request.POST.get('name'):
			section = Section.objects.get(pk=request.POST['pk'])
			section.section_name = request.POST['name']
			section.save()
		else:
			form = SectionForm(request.POST)
			if form.is_valid():
				form.save()
			else:
				return HttpResponse(str(form.errors))
		return redirect('/accounts/section')
	context = {'sections':Section.objects.all()}
	return render(request,'accounts/section.html', context)

@login_required(login_url='/loginpage')
def email(request):
	if request.method == 'POST':
		if request.POST.get('name'):
			email = EmailAccounts.objects.get(pk=request.POST['pk'])
			email.email = request.POST['name']
			if request.POST.get('active'):
				email.active = True
			else:
				email.active = False
			email.save()
		else:
			form = EmailForm(request.POST)
			if form.is_valid():
				form.save()
			else:
				return HttpResponse(str(form.errors))
		return redirect('/accounts/email')
	context = {'emails':EmailAccounts.objects.all(),'sections':Section.objects.all()}
	return render(request,'accounts/email.html',context)


@login_required(login_url='/loginpage')
def locations(request):
	if request.method == 'POST':
		if request.POST.get('name'):
			location = Location.objects.get(pk=request.POST['pk'])
			location.location_name = request.POST['name']
			location.save()
		else:
			form = LocationForm(request.POST)
			if form.is_valid():
				form.save()
			else:
				return HttpResponse(str(form.errors))
		return redirect('/accounts/locations')
	context = {'locations':Location.objects.all() }
	return render(request,'accounts/location.html', context)