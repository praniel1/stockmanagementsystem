from django.contrib import admin
from accounts.models import *

admin.site.register(User)
admin.site.register(Category)

class EntryAdmin(admin.ModelAdmin):
	list_display = ['seller_name','date_of_entry','category']
admin.site.register(Entry,EntryAdmin)

class ItemAdmin(admin.ModelAdmin):
	list_display = ['item_name','category','item_count']
admin.site.register(Item,ItemAdmin)

class IssueAdmin(admin.ModelAdmin):
	list_display = ['name','pk','designation','category','item']
admin.site.register(Issue,IssueAdmin)

# admin.site.register(StockCount,StockCountAdmin)
admin.site.register(EmailAccounts)
admin.site.register(Section)
admin.site.register(Location)