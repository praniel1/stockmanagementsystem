from django.contrib.auth.models import BaseUserManager

class UserManager(BaseUserManager):
	def create_user(self, username, password=None, is_active=True,is_staff=False, is_admin=False):
		if not username:
			raise ValueError('Users must have an username address')
		if not password:
			raise ValueError('Users must have a password')

		user_obj = self.model(
			username = self.normalize_email(username)
			)
		user_obj.set_password(password)
		user_obj.staff = is_staff
		user_obj.admin = is_admin
		user_obj.is_active = is_active
		user_obj.save(using=self._db)
		return user_obj
			
	def create_staffuser(self, username, password=None):
		user = self.create_user(username, password=password,is_staff=True)
		return user

	def create_superuser(self, username, password=None):
		user = self.create_user(username, password=password, is_staff=True, is_admin=True)
		return user

	def save(self, username, password=None):
		user = self.create_user(username, password=password, is_staff=True, is_admin=True)
		return user
