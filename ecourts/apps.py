from django.apps import AppConfig


class EcourtsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ecourts'
