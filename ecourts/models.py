from django.db import models
from accounts.models import Section,User,Location


class BulkStock(models.Model):
	received_from = models.CharField(max_length=100)
	ref_no = models.CharField(max_length=50,null=True,blank=True)
	document = models.FileField(upload_to='ecourts/document/',null=True,blank=True)
	add_document = models.FileField(upload_to='ecourts/add_document/',null=True,blank=True)
	received_date = models.DateField()
	remarks = models.TextField(null=True,blank=True)
	entered_by = models.ForeignKey(User, on_delete=models.RESTRICT,null=True)
	location = models.ForeignKey(Location,on_delete=models.RESTRICT,null=True)

	def __str__(self):
		return self.received_from

class Item(models.Model):
	description = models.CharField(max_length=500)
	sl_no = models.CharField(max_length=100)
	specification = models.CharField(max_length=500)
	status = models.CharField(max_length=20)
	reason = models.CharField(max_length=200, null=True,blank=True)
	issued = models.BooleanField(default=False)
	stock_associated = models.ForeignKey(BulkStock, on_delete=models.RESTRICT,null=True)

	def __str__(self):
		return self.description

class Issue(models.Model):
	name = models.CharField(max_length=100)
	designation = models.CharField(max_length=100)
	date = models.DateField()
	remarks = models.TextField(null=True,blank=True)
	section = models.ForeignKey(Section,on_delete=models.RESTRICT,related_name='section')
	issued_by = models.ForeignKey(User, on_delete=models.RESTRICT,related_name='issued')
	requisition = models.FileField(upload_to='ecourts/requisition/',null=True,blank=True)
	current_status = models.CharField(max_length=100,null=True,blank=True)
	item = models.ForeignKey(Item, on_delete=models.RESTRICT,related_name='item', null=True)

	def __str__(self):
		return self.name