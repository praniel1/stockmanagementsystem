from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import *
from .forms import *
from accounts.models import Section, Location
from django.core.paginator import Paginator

def index(request):
	return render(request,'ecourts/index.html')

def savetostock(request,bulkmodel):
	descriptions = request.POST.getlist('description')
	sl_no = request.POST.getlist('sl_no')
	specification = request.POST.getlist('specification')
	status = request.POST.getlist('status')
	reason = request.POST.getlist('reason')
	for i in range(0,len(descriptions)):
		item = Item()
		item.description = descriptions[i]
		item.sl_no = sl_no[i]
		item.specification = specification[i]
		item.status = status[i]
		item.reason = reason[i]
		item.stock_associated = bulkmodel
		item.save()

def addstock(request):
	if request.method == 'POST':
		bulkform = BulkStockForm(request.POST,request.FILES)
		if bulkform.is_valid():
			bulkmodel = bulkform.save()
			savetostock(request,bulkmodel)
		else:
			print(bulkform.errors)
	return render(request,'ecourts/addstock.html',{'location':Location.objects.all()})

def viewreceived(request):
	bulks = BulkStock.objects.all()
	return render(request,'ecourts/received/viewreceived.html',{'bulks':bulks})

def receiveddetail(request,pk):
	bulk = BulkStock.objects.get(pk=pk)
	items = Item.objects.filter(stock_associated=bulk)
	return render(request,'ecourts/received/receiveddetail.html',{'bulk':bulk,'items':items})

def issue(request):
	if request.method == 'POST':
		print(request.FILES)
		# return redirect('/ecourts/issue')
		request.POST._mutable = True
		request.POST['issued_by'] = request.user
		request.POST['current_status'] = request.POST['name']
		issueform = IssueForm(request.POST,request.FILES)
		if issueform.is_valid():
			issueform.save()
			item = Item.objects.get(pk=request.POST['item'])
			item.issued = True
			item.save()
			return redirect('/ecourts/issue')
		else:
			return HttpResponse(issueform.errors)
	context = {
		'bulks':BulkStock.objects.all(),
		'sections':Section.objects.all(),
	}
	return render(request,'ecourts/issue/issue.html',context)

def editbulk(request,pk):
	bulkstock = BulkStock.objects.get(pk=pk)
	if request.method == 'POST':
		bulkstock.received_from = request.POST['received_from']
		bulkstock.remarks = request.POST['remarks']
		bulkstock.save()
		itemsinstock = Item.objects.filter(stock_associated=bulkstock)
		for item in itemsinstock:
			item.delete()
		savetostock(request,bulkstock)
		return redirect('/ecourts/editbulk/'+str(bulkstock.pk))
	context = {
		'bulk':bulkstock,
		'items':Item.objects.filter(stock_associated=bulkstock)
	}
	return render(request,'ecourts/received/editbulk.html',context)

def getstockajax(request,pk):
	items = Item.objects.filter(stock_associated=BulkStock.objects.get(pk=pk))
	itemsfree = items.filter(issued=False)
	itemstaken = items.filter(issued=True)
	return render(request,'ecourts/issue/ajaxissuedata.html',{'itemsfree':itemsfree,'itemstaken':itemstaken})

def viewallissued(request):
	issued = Issue.objects.all().order_by('-date')
	paginator = Paginator(issued, 12)
	page = request.GET.get('page')
	issued = paginator.get_page(page)
	return render(request,'ecourts/issue/viewallissued.html',{'issued':issued})

def detailissued(request,pk):
	if request.method == 'POST':
		issue = Issue.objects.get(pk=pk)
		issue.current_status = request.POST['current_status']
		issue.save()
		return redirect('/ecourts/detailissued/'+str(pk))
	issued = Issue.objects.get(pk=pk)
	return render(request,'ecourts/issue/detailissued.html',{'issued':issued})