from django.contrib import admin
from .models import *

admin.site.register(Item)
admin.site.register(Issue)
admin.site.register(BulkStock)