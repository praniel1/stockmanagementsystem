from django.urls import path
from .import views

app_name='ecourts'

urlpatterns = [
	path('',views.index, name='index'),
	path('addstock/',views.addstock,name='addstock'),
	path('viewreceived/',views.viewreceived, name='viewreceived'),
	path('receiveddetail/<int:pk>',views.receiveddetail,name='receiveddetail'),
	path('issue',views.issue, name='issue'),
	path('editbulk/<int:pk>',views.editbulk, name='editbulk'),
	path('getstockajax/<int:pk>',views.getstockajax, name='getstockajax'),
	path('viewallissued/',views.viewallissued,name='viewallissued'),
	path('detailissued/<int:pk>',views.detailissued,name='detailissued'),
]