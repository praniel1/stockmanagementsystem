from django import forms
from .models import BulkStock,Item,Issue


class BulkStockForm(forms.ModelForm):
	class Meta:
		model = BulkStock
		fields = '__all__'

class ItemForm(forms.ModelForm):
	class Meta:
		model = Item
		fields = '__all__'

class IssueForm(forms.ModelForm):
	class Meta:
		model = Issue
		fields = '__all__'