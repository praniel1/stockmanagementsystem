from django import template
# from django.utils.safestring import mark_safe
# from accounts.models import Category,Item
register = template.Library()
import os 
import json
from ecourts.models import *

@register.filter
def filename(value):
	return os.path.basename(value.file.name)

@register.filter
def file_exists(value):
	print(value)
	if value.storage.exists(value.name):
		return True
	else:
		return False

@register.filter
def getissuedtoperson(value):
	item = Item.objects.get(pk=value)
	issue = Issue.objects.get(item=item)
	return issue.name