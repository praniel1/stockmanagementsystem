from django.shortcuts import render,redirect
from django.http import HttpResponse
from accounts.models import *
from .forms import *
from accounts.form import IssueForm
from django.core.paginator import Paginator
from .otherviewfunctions import *
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from datetime import datetime

@login_required(login_url='/loginpage')
def index(request):
	if request.user.is_authenticated:
		# if request.user.is_superuser:
		# 	categories=Category.objects.all()
		# else:
		# 	categories=Category.objects.filter(section=request.user.section)
		categories = Category.objects.filter(section=request.user.section,location=request.user.location)
	else:
		categories=Category.objects.all()
	return render(request,'dashboard/index.html', {'categories':categories})

def categoryview(request,categoryname):
	return render(request,'dashboard/viewcategory.html',{'categoryname':categoryname})

@login_required(login_url='/loginpage')
def issue(request,categoryname):
	if request.method == 'POST':
		form = IssueForm(compileissuedata(request),request.FILES)
		if form.is_valid():
			instance = form.save()
			messages.success(request, 'Items issued with id : '+str(instance.pk))
			return redirect('/category/'+categoryname)
		else:
			return HttpResponse(form.errors)
	return render(request,'dashboard/issue/issue.html',getcontext(categoryname))

@login_required(login_url='/loginpage')
def addstock(request,categoryname):
	if request.method == 'POST':
		form = EntryForm(compileaddstockdata(request),request.FILES)
		if form.is_valid():
			instance = form.save()
			sendstockaddmail(instance,request)
			messages.success(request, 'Stock added with id : '+str(instance.pk))
			return redirect('/category/'+categoryname)
		else:
			return HttpResponse(str(form.errors))
	return render(request,'dashboard/purchase/addstock.html',getcontext(categoryname))

def getstockajax(request):
	itempk = request.GET['itempk']
	item = Item.objects.get(pk=itempk)
	return HttpResponse(item.item_count)

def viewstock(request,categoryname):
	items = Item.objects.filter(category=Category.objects.get(category_name=categoryname))
	context = {'categoryname':categoryname,'items':items,}
	return render(request,'dashboard/viewstock.html',context)

def viewissued(request,categoryname):
	category = Category.objects.get(category_name=categoryname)
	issues = Issue.objects.filter(category=category).order_by('-date_of_issue')
	if request.GET.get('showreport'):
		issues = Issue.objects.filter(category=category).order_by('date_of_issue')
	searchfields = ['name','designation','item','section','issued_by','start_date','end_date']
	for field in searchfields:
		if request.GET.get(field):
			issues = searchissues(issues,request)
			break
	if request.GET.get('showreport'):
		templatename = 'dashboard/issue/report_issue.html'
		return render(request,templatename,
			givereportcontext(request,searchfields,issues,categoryname))
	context = getcontext(categoryname)
	paginator = Paginator(issues, 12)
	page = request.GET.get('page')
	issues = paginator.get_page(page)
	context['paginationdata'] = context['issued'] = issues
	return render(request,'dashboard/issue/viewissued.html',context)

def viewpurchased(request,categoryname):
	category = Category.objects.get(category_name=categoryname)
	entrys = Entry.objects.filter(category=category).order_by('-pk')
	if request.GET.get('showreport'):
		entrys = Entry.objects.filter(category=category)
	searchfields = ['item','entered_by','start_date','end_date','ref_no']
	for field in searchfields:
		if request.GET.get(field):
			entrys = searchentrys(entrys,request)
	if request.GET.get('showreport'):
		context = {'entrys':entrys,'categoryname':categoryname}
		return render(request,'dashboard/purchase/report_purchase.html',context)
	context = getcontext(categoryname)
	paginator = Paginator(entrys, 12)
	page = request.GET.get('page')
	entrys = paginator.get_page(page)
	context['entrys'] = context['paginationdata'] = entrys
	return render(request,'dashboard/purchase/viewpurchased.html',context)

def usage(request,categoryname,usagetype):
	if usagetype == 'itemandsection':
		items = Item.objects.filter(category=Category.objects.get(category_name=categoryname))
		itemsandusage = produceusagetypeboth(request,items)
		context = {
			'categoryname':categoryname,
			'itemsandusage':itemsandusage,
			'usagetype':usagetype,
			'items':items,
		}
		return render(request,'dashboard/usage/usageboth.html',context)
	
	if usagetype == 'section':
		usages = {}
		sections = Section.objects.all()
		categoryobj = Category.objects.get(category_name=categoryname)
		for section in sections:
			issues =  Issue.objects.filter(category=categoryobj,section=section)
			if request.GET.get('from'):
				issues = issues.filter(date_of_issue__gte=request.GET['from'],
				date_of_issue__lte=request.GET['to'])
			issues = issues.filter(section=section)
			sectionuse = 0
			for issue in issues:
				issueitem = issue.item
				subcatstring = issueitem.replace("\'","\"")
				subcatstring = json.loads(subcatstring)
				for key, value in subcatstring.items():
					sectionuse+=int(value)
			usages[section.section_name] = sectionuse

	if usagetype == 'item':
		usages = {}
		items = Item.objects.filter(category__category_name=categoryname)
		issues = Issue.objects.all()
		if request.GET.get('from'):
			issues = issues.filter(date_of_issue__gte=request.GET['from'],
				date_of_issue__lte=request.GET['to'])
		for item in items:		
			itemuse = 0
			for issue in issues:
				issueitem = issue.item
				subcatstring = issueitem.replace("\'","\"")
				subcatstring = json.loads(subcatstring)
				if str(item.pk) in subcatstring:
					itemuse += int(subcatstring[str(item.pk)])
			usages[item.item_name] = itemuse
	total = 0
	for key,value in usages.items():
		total += value
	context = {'usages':usages,'categoryname':categoryname,'usagetype':usagetype,'total':total}
	return render(request,'dashboard/usage/usage.html',context)

def viewdetailpurchase(request,pk):
	entry = Entry.objects.get(pk=pk)
	context = {'entry':entry,'category':entry.category}
	return render(request,'dashboard/purchase/viewdetailpurchased.html',context)

def viewdetailissued(request,pk):
	issue = Issue.objects.get(pk=pk)
	context = {'issue':issue,'category':issue.category}
	return render(request,'dashboard/issue/viewdetailissued.html',context)

def usagereport(request,categoryname):
	itemspk = request.GET.getlist('item')
	if len(itemspk)>0:
		items = Item.objects.filter(pk__in = itemspk)
	else:
		items = Item.objects.filter(category=Category.objects.get(category_name=categoryname))
	itemsandusage = produceusagetypeboth(request,items)
	context = {
		'itemsandusage':itemsandusage,
		'categoryname':categoryname,
	}
	if request.GET.get('from'):
		date_object = datetime.strptime(request.GET.get('from'), '%Y-%m-%d').date()
		fromdate = date_object.strftime('%d-%b-%Y')
		date_object = datetime.strptime(request.GET.get('to'), '%Y-%m-%d').date()
		todate = date_object.strftime('%d-%b-%Y')
		context['duration']='from '+str(fromdate)+' to '+str(todate)
	else:
		context['duration'] = 'all time'
	return render(request,'dashboard/usage/usagereportboth.html',context)

def deleteissue(request,pk):
	issue = Issue.objects.get(pk=pk)
	issueitem = issue.item
	subcatstring = issueitem.replace("\'","\"")
	subcatstring = json.loads(subcatstring)
	for key,value in subcatstring.items():
		# key ->item, value->quantity
		item = Item.objects.get(pk=key)
		count = item.item_count
		count+=int(value)
		item.item_count = count
		item.save()
	category = issue.category.category_name
	issue.delete()
	return redirect('/view-issued/'+category)

def changeassignedlocation(request):
	user = request.user
	location = Location.objects.get(pk=request.GET['location'])
	user.location = location
	user.save()
	return redirect(request.GET['currentpage'])