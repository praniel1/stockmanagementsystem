from django import template
from django.utils.safestring import mark_safe
from accounts.models import Category,Item, Location
register = template.Library()
import os 
import json
from django.http import HttpResponse

@register.filter
def placecategory(ss):
	categories = Category.objects.all()
	htmlvariable = ''
	for category in categories:
		htmlvariable+='<li class="nav-item"><a class="nav-link active" aria-current="page" href="/dashboard/category/'+category.category_name+'">'+category.category_name+'</a></li>'
	return htmlvariable
	
@register.simple_tag
def url_replace(request, field=None, value=None):
    dict_ = request.GET.copy()
    if field != None:
        dict_[field] = value
    return dict_.urlencode()

@register.filter
def filename(value):
	return os.path.basename(value.file.name)

@register.filter
def file_exists(value):
	print(value)
	if value.storage.exists(value.name):
		return True
	else:
		return False

@register.filter
def getitemname(pk):
	item = Item.objects.get(pk=pk)
	return item.item_name

@register.filter
def showitemname(string):
	subcatstring = string.replace("\'","\"")
	subcatstring = json.loads(subcatstring)
	stringarr = []
	for key,value in subcatstring.items():
		itemname = Item.objects.get(pk=key)
		if int(value) > 1:
			stringarr.append(itemname.item_name+'('+value+'nos)')
		else:
			stringarr.append(itemname.item_name)
	return ','.join(stringarr)

@register.filter
def stringtosubcatconvert(string,issueorpurchase=''):
	subcatstring = string.replace("\'","\"")
	subcatstring = json.loads(subcatstring)
	if issueorpurchase == '':
		tablestring = '<tr class="insidetr"><td class="insidetd">Item</td><td>nos</td></tr>'
		for key,value in subcatstring.items():
			itemname = Item.objects.get(pk=key)
			tablestring+= '<tr class="insidetr"><td class="insidetd">'+itemname.item_name+'</td><td>'+value+'</td></tr>'
	else:
		tablestring = '<tr class="insidetr"><td  class="insidetd" style="padding:1px;">Item</td><td style="padding:1px;">nos</td></tr>'
		for key,value in subcatstring.items():
			itemname = Item.objects.get(pk=key)
			tablestring+= '<tr class="insidetr"><td  class="insidetd" style="padding:1px;">'+itemname.item_name+'</td><td style="padding:1px;">'+value+'</td></tr>'
	return tablestring

@register.filter
def keysconvert(string):
	return string.replace('_',' ').title()

# @register.inclusion_tag('base.html')
@register.filter()
def putlocationoption(userlocation):
	string = '<select name="location" class="form-control" onchange="this.form.submit()">'
	locations = Location.objects.all()
	for location in locations:
		string += '<option value="'+ str(location.pk)+'"'
		if userlocation == location :
			string += 'selected' 
		string += '>'+str(location.location_name)+'</option>'
	string += '</select>'
	# print(string)
	return string