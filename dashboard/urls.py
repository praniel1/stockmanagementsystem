from django.urls import path
from .import views

app_name='dashboard'

urlpatterns = [
	path('',views.index, name='index'),
	path('category/<str:categoryname>',views.categoryview, name='categoryview'),
	path('issue/<str:categoryname>', views.issue, name='issue'),
	path('addstock/<str:categoryname>', views.addstock, name='addstock'),
	path('viewstock/<str:categoryname>',views.viewstock, name='viewstock'),
	path('getstockajax',views.getstockajax,name='getstock'),
	path('view-issued/<str:categoryname>',views.viewissued, name='viewissued'),
	path('view-purchased/<str:categoryname>', views.viewpurchased,name='viewpurchased'),
	path('usage/<str:categoryname>/<str:usagetype>', views.usage, name='usage'),
	path('viewdetailpurchase/<int:pk>',views.viewdetailpurchase,name='viewdetailpurchase'),
	path('viewdetailissued/<int:pk>',views.viewdetailissued,name='viewdetailissued'),
	path('usagereport/<str:categoryname>',views.usagereport,name='usagereport'),
	path('deleteissue/<int:pk>',views.deleteissue,name='deleteissue'),
	path('changeassignedlocation', views.changeassignedlocation, name='changeassignedlocation')
]