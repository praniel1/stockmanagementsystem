from django import forms
from accounts.models import *


class EntryForm(forms.ModelForm):
	class Meta:
		model = Entry
		fields = '__all__'