from accounts.models import *
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import send_mail, EmailMessage
import ast
import json

def searchissues(issues,request):
	if request.GET.get('name'):
		issues = issues.filter(name__icontains=request.GET['name'])
	if request.GET.get('designation'):
		issues = issues.filter(designation__icontains=request.GET['designation'])
	if request.GET.get('section'):
		issues = issues.filter(section=Section.objects.get(section_name=request.GET['section']))
	if request.GET.get('issued_by'):
		issues = issues.filter(issued_by=User.objects.get(username=request.GET['issued_by']))
	if request.GET.get('start_date'):
		issues = issues.filter(date_of_issue__gte=request.GET['start_date'])
	if request.GET.get('end_date'):
		issues = issues.filter(date_of_issue__lte=request.GET['end_date'])
	if request.GET.get('item'):
		itemid = Item.objects.get(item_name__iexact=request.GET['item']).pk
		issuedids = []
		for issue in issues:
			issuedict = issue.item
			subcatstring = issuedict.replace("\'","\"")
			subcatstring = json.loads(subcatstring)
			if str(itemid) in subcatstring:
				issuedids.append(issue.id)
		issues = issues.filter(pk__in=issuedids)
	return issues

def searchentrys(entrys,request):
	if request.GET.get('ref_no'):
		entrys = entrys.filter(ref_no__contains=request.GET['ref_no'])
	if request.GET.get('entered_by'):
		entrys = entrys.filter(entered_by=User.objects.get(username=request.GET['entered_by']))
	if request.GET.get('start_date'):
		entrys = entrys.filter(date_of_entry__gte=request.GET['start_date'])
	if request.GET.get('end_date'):
		entrys = entrys.filter(date_of_entry__lte=request.GET['end_date'])
	if request.GET.get('item'):
		itemid = Item.objects.get(item_name__iexact=request.GET['item']).pk
		entryids = []
		for entry in entrys:
			entrydic = entry.item
			subcatstring = entrydic.replace("\'","\"")
			subcatstring = json.loads(subcatstring)
			if str(itemid) in subcatstring:
				entryids.append(entry.id)
		entrys = entrys.filter(pk__in=entryids)
	return entrys

def givereportcontext(request,searchfields,issues,categoryname):
	fieldsandvalues = {} # sending searched fields and values to print in report
	fieldssearchby = [] # sending searched fields to report to check for reducing no. of columns
	for field in searchfields:
		if request.GET.get(field):
			issues = searchissues(issues,request)
			if ((field == 'start_date') or (field == 'end_date')):
				pass
			else:
				fieldsandvalues[field]=request.GET[field]
				fieldssearchby.append(field)
	context = {'issued':issues,'categoryname':categoryname,'fieldsandvalues':fieldsandvalues,'fieldssearchby':fieldssearchby}
	return context

def getcontext(categoryname):
	context = {
		'category':Category.objects.get(category_name=categoryname),
		'items':Item.objects.filter(category__category_name=categoryname),
		'sections':Section.objects.all(),
		'users':User.objects.all(),
	}
	return context

def compileissuedata(request):
	itemdict = {}
	itemslist = request.POST.getlist('item')
	issuenos = request.POST.getlist('issue_nos')
	for i, item in enumerate(itemslist):
		itemobj = Item.objects.get(pk=item)
		itemobj.item_count -= int(issuenos[i])
		itemobj.save()
		if(itemobj.item_count<3):
				sendissuemail(itemobj,request)
		itemdict[item] = issuenos[i]
	post_request = request.POST
	post_request._mutable = True
	post_request['item'] = itemdict
	return post_request

def compileaddstockdata(request):
	itemdict = {}
	item_quantity_list = request.POST.getlist('item_quantity')
	itemlist = request.POST.getlist('item')
	for i, item in enumerate(itemlist):
		itemobj = Item.objects.get(pk=item)
		itemobj.item_count += int(item_quantity_list[i])
		itemobj.save()
		itemdict[item]=item_quantity_list[i]
	post_request = request.POST
	post_request._mutable = True
	post_request['item'] = itemdict
	return post_request

def sendstockaddmail(instance,request):
	subject = 'New stock added'
	itemdetails = instance.item.replace("\'","\"")
	itemdetails = json.loads(itemdetails)
	html_message = render_to_string('emailtemplates/stockadded.html',{'stock':instance,'itemdetails':itemdetails})
	plain_message = strip_tags(html_message)
	from_email = 'stockmanagementsystem@gmail.com'
	emails = EmailAccounts.objects.filter(active=True,section=instance.category.section)
	emaillist = []
	for email in emails:
		emaillist.append(email.email)
	send_mail(subject,plain_message,from_email,emaillist ,html_message=html_message)

def sendissuemail(stockcount,request):
	subject = 'Stock shortage'
	html_message = render_to_string('emailtemplates/stockshortage.html',{'stock':stockcount})
	from_email = 'stockmanagementsystem@gmail.com'
	plain_message = strip_tags(html_message)
	emails = EmailAccounts.objects.filter(active=True,section=stockcount.category.section)
	emaillist = []
	for email in emails:
		emaillist.append(email.email)
	send_mail(subject,plain_message,from_email,emaillist ,html_message=html_message)

def produceusagetypeboth(request,items):
	sections = Section.objects.all()
	itemsandusage = {}
	for item in items:	
		usagesdict = {}
		for section in sections:
			totalsectionuse = 0
			issues = Issue.objects.filter(section=section)
			if request.GET.get('from'):
				issues = issues.filter(date_of_issue__gte=request.GET['from'],
						date_of_issue__lte=request.GET['to'])
			for issue in issues:
				issueitem = issue.item
				subcatstring = issueitem.replace("\'","\"")
				subcatstring = json.loads(subcatstring)
				for key,value in subcatstring.items():
					if int(key) == item.pk:
						totalsectionuse += int(value)
			usagesdict[section.section_name] = totalsectionuse
		itemsandusage[item.item_name] = usagesdict
	return itemsandusage